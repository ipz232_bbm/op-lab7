#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {

	double x, y;

	printf("x = ");
	scanf("%lf", &x);
	printf("y = ");
	scanf("%lf", &y);
	

	if ((y <= x + 1 && x <= 0 && y <= 0) || (y >= -x + 1 && x <= 0) || (y <= -x + 1 && x >= 0 && y >= 0)) printf("true");
	else printf("false");

	return 0;
}